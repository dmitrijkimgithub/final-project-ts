# Shopping List App

This is a simple shopping list web application built with TypeScript and Vite.

## Prerequisites

Before running the project, ensure you have the following installed:

- Node.js (version v20.5.1)
- npm (Node Package Manager)

## Installation

1. Clone the repository to your local machine using Terminal:

```bash

git clone https://gitlab.com/dmitrijkimgithub/final-project-ts.git

```

2. Install dependencies:

```bash

npm install

```

3. To run the project in development mode, use the following command:

```bash

npm run dev

```

This will start the development server and open the project in your default web browser locally. You have to follow the link given to you like this -- http://localhost:5173/

**(P.S.)** In case this does not work, then you should install Vite separately using this command in Terminal:

```bash

npm i vite@latest

```

4. To build the project for production, run the following command:

```bash

npm run build

```

5. To run tests, use the following command:

```bash

npm run test

```