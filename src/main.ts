import { Lists, List, Item } from './interfaces';
import { sidebar, modal, nav, content, header, body } from './variables';

export class ShoppingList {
  shoppingLists: Lists;
  isNew: boolean = false;
  currentList: List | null = null;

  constructor() {
    this.shoppingLists = this.loadListsFromStorage();
    this.currentList = this.getCurrentList();
    if (!this.currentList && this.shoppingLists.lists.length > 0) {
      this.currentList = this.shoppingLists.lists[0];
    } else if (!this.currentList) {
      this.currentList = {
        id: this.shoppingLists.count,
        title: 'New List',
        idItem: 0,
        items: [],
        categories: []
      };
      this.shoppingLists.lists.push(this.currentList);
      this.shoppingLists.count += 1;
    }
    this.renderAll();
    this.setupEventListeners();
  }

  getCurrentList(): List | null {
    return this.shoppingLists.lists.find(list => list.id === this.shoppingLists.currentId) || null;
  }

  loadListsFromStorage(): Lists {
    const savedLists = localStorage.getItem('shoppingLists');
    if (savedLists) {
      return JSON.parse(savedLists);
    }
    return { lists: [], count: 0, currentId: null };
  }
  renderAll() {
    this.renderLists();
    this.renderItems();
    this.renderCategories();
    this.saveToLocalStorage();

    if (!this.currentList && header.headerTitle) {
      header.headerTitle.textContent = ""
      modal.modalBtnCancel.disabled = true;
      this.openModalCreate()
    }
  }

  renderLists() {
    if (sidebar.sidebarList) {
      sidebar.sidebarList.innerHTML = '';
      this.shoppingLists.lists.forEach(list => this.renderList(list));
    }
  }

  renderList(list: List) {
    const listItem = document.createElement('li');
    listItem.classList.add('sidebar__list');
    if (list.id === this.shoppingLists.currentId) {
      listItem.classList.add('active');
      if (header.headerTitle) header.headerTitle.textContent = list.title;
    }
    listItem.addEventListener('click', () => this.changeList(list.id));

    const iconItem = document.createElement('i');
    iconItem.setAttribute('aria-hidden', 'true');
    iconItem.classList.add('fa', 'fa-check', 'sidebar__icon');

    const textItem = document.createElement('span');
    textItem.classList.add('sidebar__text');
    textItem.textContent = list.title;

    listItem.appendChild(iconItem);
    listItem.appendChild(textItem);
    sidebar.sidebarList.appendChild(listItem);
  }

  renderItems() {
    if (content.contentList && this.currentList) {
      content.contentList.innerHTML = '';
      this.currentList.items.forEach(item => this.renderItem(item));
    } else if (content.contentList) {
      content.contentList.innerHTML = '';
    }
  }

  renderItem(item: Item) {
    const listItem = document.createElement('li');
    listItem.classList.add('list__item');

    const productDiv = document.createElement('div');
    productDiv.classList.add('list__product');

    const checkboxDiv = document.createElement('div');
    checkboxDiv.classList.add('checkbox-wrapper-19');

    const checkboxInput = document.createElement('input');
    checkboxInput.type = 'checkbox';
    checkboxInput.classList.add('list__product-checkbox');
    checkboxInput.id = item.id.toString();
    checkboxInput.checked = item.isChecked;
    checkboxInput.addEventListener('change', () => this.checkItem(item));

    const checkboxLabel = document.createElement('label');
    checkboxLabel.classList.add('check-box');
    checkboxLabel.htmlFor = item.id.toString();

    const productName = document.createElement('p');
    productName.classList.add('list__product-name');
    productName.textContent = `${item.name} × `;

    const productQuantity = document.createElement('span');
    productQuantity.classList.add('list__product-quantity');
    productQuantity.textContent = item.quantity.toString();

    productName.appendChild(productQuantity);

    const productCategory = document.createElement('div');
    productCategory.classList.add('list__product__category');
    productCategory.textContent = item.category || 'Uncategorized';

    const deleteButton = document.createElement('button');
    deleteButton.classList.add('btn', 'btn-delete', 'ml');
    deleteButton.textContent = 'Delete';
    deleteButton.addEventListener('click', () => this.removeItem(item));

    productDiv.appendChild(checkboxInput);
    productDiv.appendChild(checkboxLabel);
    productDiv.appendChild(productName);
    productDiv.appendChild(productCategory);
    productDiv.appendChild(deleteButton);

    listItem.appendChild(productDiv);
    content.contentList.appendChild(listItem);
  }

  renderCategories() {
    if (nav.navSelectCategory && this.currentList) {
      nav.navSelectCategory.innerHTML = '';
      const allCategoriesOption = document.createElement('option');
      allCategoriesOption.value = 'all';
      allCategoriesOption.textContent = 'All Categories';
      nav.navSelectCategory.appendChild(allCategoriesOption);

      this.currentList.categories.forEach(category => this.addCategoryToSelect(category));
    }
  }

  setupEventListeners() {
    header.headerBtnDelete?.addEventListener('click', () => this.deleteList());
    header.headerBtnEdit?.addEventListener('click', () => this.openModalEdit());
    header.headerBtnToggle?.addEventListener('click', () => this.toggleSidebar());
    sidebar.sidebarBtnCreate?.addEventListener('click', () => this.openModalCreate());
    modal.modalForm?.addEventListener('submit', (e: Event) => this.formHandler(e));
    modal.modalBtnCancel?.addEventListener('click', () => this.closeModal());
    sidebar.sidebarBtnClose?.addEventListener('click', () => this.closeSidebar());
    nav.navSelectCategory?.addEventListener('change', () => this.filterListByCategory());
    nav.navBtnAdd?.addEventListener('click', () => this.addItemFromInput());
  }

  checkItem(item: Item) {
    item.isChecked = !item.isChecked;
    this.saveToLocalStorage();
  }

  saveToLocalStorage() {
    localStorage.setItem('shoppingLists', JSON.stringify(this.shoppingLists));
  }

  filterByCategory(category: string) {
    return this.currentList?.items.filter((item) => item.category === category)
  }

  filterListByCategory() {
    const selectedCategory = nav.navSelectCategory?.value;
    const listItems = content.contentList?.querySelectorAll('.list__item');

    listItems?.forEach(item => {
      const itemCategory = (item.querySelector('.list__product__category') as HTMLElement)?.textContent?.trim();
      if (itemCategory) {
        (item as HTMLElement).style.display = selectedCategory === 'all' || itemCategory === selectedCategory ? 'block' : 'none';
      }
    });
  }

  addCategoryToSelect(category: string) {
    const option = document.createElement('option');
    option.value = category || 'uncategorized';
    option.textContent = category || 'Uncategorized';
    nav.navSelectCategory?.appendChild(option);
  }

  addItemFromInput() {
    const name = nav.navInputName?.value.trim();
    const quantity = parseInt(nav.navInputQuantity?.value.trim());

    if (!name || isNaN(quantity)) {
      alert('Please enter a name and quantity for the item.');
      return;
    }

    const id = this.currentList?.idItem ?? 0;
    const category = nav.navInputCategory?.value.trim();

    const newItem = new Item(id, name, quantity, false, category);
    this.addItem(newItem)

    nav.navInputName.value = '';
    nav.navInputQuantity.value = '';
    nav.navInputCategory.value = '';

    this.saveToLocalStorage();
    this.renderAll();
  }

  addItem(newItem: Item) {
    if (!newItem.category) {
      newItem.category = 'Uncategorized'
    }

    this.currentList?.items.push(newItem);
    if (this.currentList) this.currentList.idItem += 1;

    if (this.currentList && !this.isCategoryExists(newItem.category)) {
      this.addCategoryToSelect(newItem.category);
      this.currentList.categories.push(newItem.category);
      this.saveToLocalStorage();
    }
  }

  isCategoryExists(category: string): boolean {
    return this.currentList?.categories.includes(category) ?? false;
  }

  removeCategoryFromSelect(category: string) {
    const options = nav.navSelectCategory?.options;
    for (let i = 0; i < options?.length; i++) {
      if (options[i].value === category) {
        nav.navSelectCategory?.removeChild(options[i]);
        break;
      }
    }
  }

  removeItem(item: Item) {
    const items = this.currentList?.items || [];
    const index = items.indexOf(item);

    if (index !== -1) {
      items.splice(index, 1);
      content.contentList?.querySelector(`[data-id="${item.id}"]`)?.remove();

      if (item.category) {
        const categoryItemCount = items.filter(i => i.category === item.category).length;
        if (categoryItemCount === 0) {
          const categoryIndex = this.currentList?.categories.indexOf(item.category) ?? -1;
          if (categoryIndex !== -1) {
            this.currentList?.categories.splice(categoryIndex, 1);
            this.removeCategoryFromSelect(item.category);
          }
        }
      }

      this.renderItems();
      this.saveToLocalStorage();
    }
  }

  openModalCreate() {
    modal.overlay?.classList.add('active');
    body.classList.add('active');
    if (modal.modalTitle) modal.modalTitle.textContent = 'Create';
    this.isNew = true;
  }

  openModalEdit() {
    modal.overlay?.classList.add('active');
    body.classList.add('active');
    if (modal.modalTitle) modal.modalTitle.textContent = 'Edit';
  }

  toggleSidebar() {
    header.header.classList.toggle('active');
    header.headerMain.classList.toggle('active')
    header.headerBox.classList.toggle('active')
    sidebar.sidebar.classList.toggle('active')
    header.headerBtnToggle.classList.toggle('active')
  }

  closeSidebar() {
    sidebar.sidebar.classList.remove('active')
    header.headerBtnToggle.classList.remove('active')
  }

  closeModal() {
    if (modal.modalTitle) modal.modalTitle.textContent = '';
    if (modal.modalInputText) modal.modalInputText.value = '';
    this.isNew = false;
    modal.overlay?.classList.remove('active');
    body.classList.remove('active');

    if (!this.currentList) {
      this.openModalCreate()
    }
  }

  formHandler(e: Event) {
    e.preventDefault();
    this.isNew ? this.saveNewList() : this.editList();
  }

  saveNewList() {
    const title = modal.modalInputText?.value.trim() || 'New List';
    const id = this.shoppingLists.count;
    const newList: List = { id, title, idItem: 0, items: [], categories: [] };

    this.shoppingLists.lists.unshift(newList);
    this.shoppingLists.currentId = id;
    this.shoppingLists.count += 1;

    this.currentList = this.getCurrentList();
    modal.modalBtnCancel.disabled = false;
    this.renderAll();
    this.closeModal();
  }

  editList() {
    const title = modal.modalInputText?.value.trim() || 'New List';
    const id = this.shoppingLists.currentId!;

    const editedListIndex = this.shoppingLists.lists.findIndex(list => list.id === id);

    if (editedListIndex !== -1) {
      this.shoppingLists.lists[editedListIndex].title = title;
      this.renderAll();
      this.saveToLocalStorage();
      this.closeModal();
    }
  }

  deleteList() {
    const index = this.shoppingLists.lists.findIndex(list => list.id === this.currentList?.id);

    if (index !== -1) {
      this.shoppingLists.lists.splice(index, 1);
      if (this.shoppingLists.lists.length > 0) {
        this.shoppingLists.currentId = this.shoppingLists.lists[0].id;
        this.currentList = this.getCurrentList();
      } else {
        this.shoppingLists.currentId = null;
        this.currentList = null;
      }
      this.renderAll();
      this.saveToLocalStorage();
    }
  }

  changeList(id: number) {
    if (this.shoppingLists.currentId !== id) {
      this.shoppingLists.currentId = id;
      this.currentList = this.getCurrentList();
      this.renderAll();
    }
  }
}

const shoppingList = new ShoppingList();
