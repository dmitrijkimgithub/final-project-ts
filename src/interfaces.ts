export class Item {
  id: number;
  name: string;
  quantity: number;
  isChecked: boolean;
  category?: string;

  constructor(id: number, name: string, quantity: number, isChecked: boolean, category?: string) {
    this.id = id;
    this.name = name;
    this.quantity = quantity;
    this.isChecked = isChecked;
    this.category = category;
  }

  toggleChecked(): void {
    this.isChecked = !this.isChecked;
  }
}

export interface List {
  id: number;
  title: string;
  idItem: number;
  items: Item[];
  categories: string[];
}

export interface Lists {
  lists: List[];
  count: number;
  currentId: number | null;
}
