import { ShoppingList } from "./main"
import { Item } from './interfaces';

describe("Item class", () => {
  it("should create an instance of Item", () => {
    const newItem: Item = new Item(1, "Apple", 5, false, "Fruit")
    expect(newItem).toBeInstanceOf(Item)
    expect(newItem.name).toBe("Apple")
    expect(newItem.quantity).toBe(5)
    expect(newItem.isChecked).toBe(false)
    expect(newItem.category).toBe("Fruit")
  })

  it("should toggle isChecked property", () => {
    const item = new Item(2, "Milk", 1, false)
    expect(item.isChecked).toBe(false)
    item.isChecked = true
    expect(item.isChecked).toBe(true)
  })
})

describe("ShoppingList", () => {
  let shoppingList: ShoppingList
  let addButton: HTMLButtonElement
  let nameInput: HTMLInputElement
  let quantityInput: HTMLInputElement
  let categoryInput: HTMLInputElement
  let categoryContainer: HTMLSelectElement
  let listContainer: HTMLUListElement

  beforeEach(() => {
    // Create and append necessary DOM elements
    document.body.innerHTML = `
        <button class="btn-add"></button>
        <input id="name" type="text" />
        <input id="quantity" type="number" />
        <input id="category" type="text" />
        <select id="categoryFilter"></select>
        <ul class="main__content-list"></ul>
      `

    // Initialize variables with the created elements
    addButton = document.querySelector(".btn-add") as HTMLButtonElement
    nameInput = document.getElementById("name") as HTMLInputElement
    quantityInput = document.getElementById("quantity") as HTMLInputElement
    categoryInput = document.getElementById("category") as HTMLInputElement
    categoryContainer = document.getElementById(
      "categoryFilter"
    ) as HTMLSelectElement
    listContainer = document.querySelector(
      ".main__content-list"
    ) as HTMLUListElement

    // Create a new instance of ShoppingList
    shoppingList = new ShoppingList()
  })

  afterEach(() => {
    // Clear localStorage after each test
    localStorage.clear()
  })

  it("should initialize with empty items and categories", () => {
    expect(shoppingList.currentList?.items).toHaveLength(0)
    expect(shoppingList.currentList?.categories).toHaveLength(0)
  })

  it("should add an item to the list", () => {
    const newItem = new Item(5, "Apple", 5, false, "Fruits")
    shoppingList.addItem(newItem)
    expect(shoppingList.currentList?.items).toContain(newItem)
  })

  it("should remove an item from the list", () => {
    const newItem = new Item(10, "Apple", 5, false, "Fruits")
    shoppingList.addItem(newItem)
    shoppingList.removeItem(newItem)
    expect(shoppingList.currentList?.items).not.toContain(newItem)
  })

  it("should filter items by category", () => {
    const item1 = new Item(11, "Apple", 5, false, "Fruit")
    const item2 = new Item(12, "Milk", 1, false, "Dairy")
    shoppingList.addItem(item1)
    shoppingList.addItem(item2)
    const filteredItems = shoppingList.filterByCategory("Fruit")
    expect(filteredItems?.length).toBe(1)
    expect(filteredItems?.[0]).toEqual(item1)
  })

  it("should add category to the list of categories", () => {
    const category = "Vegetables"
    shoppingList.addItem(new Item(12, "Carrot", 3, false, category))
    expect(shoppingList.currentList?.categories.includes(category)).toBe(true)
  })

  it("should not duplicate category in the list of categories", () => {
    const category = "Fruit"
    shoppingList.addItem(new Item(13, "Apple", 5, false, category))
    shoppingList.addItem(new Item(14, "Banana", 3, false, category))
    expect(
      shoppingList.currentList?.categories.filter((cat) => cat === category).length
    ).toBe(1)
  })

  it("should remove category from the list of categories when all associated items are removed", () => {
    const category = "Fruit"
    shoppingList.addItem(new Item(15, "Apple", 5, false, category))
    shoppingList.removeItem(shoppingList.currentList!.items[0])
    expect(shoppingList.currentList?.categories.includes(category)).toBe(false)
  })

  it("should add a category when adding an item with a new category", () => {
    const newItem = new Item(16, "Apple", 5, false, "Fruits")
    shoppingList.addItem(newItem)
    expect(shoppingList.currentList?.categories).toContain("Fruits")
  })

  it("should remove category from select options when no items are associated with it", () => {
    shoppingList.addItem(new Item(17, "Tomato", 2, false, "Vegetables"))
    shoppingList.removeItem(shoppingList.currentList!.items[0])
    expect(shoppingList.currentList?.categories.includes("Vegetables")).toBe(false)
  })

  it("should handle toggleChecked method", () => {
    const item = new Item(17, "Bread", 1, false)
    item.toggleChecked()
    expect(item.isChecked).toBe(true)
    item.toggleChecked()
    expect(item.isChecked).toBe(false)
  })

  it("should handle filtering when no category is selected", () => {
    const item1 = new Item(18, "Apple", 5, false, "Fruit")
    const item2 = new Item(19, "Milk", 1, false, "Dairy")
    shoppingList.addItem(item1)
    shoppingList.addItem(item2)
    shoppingList.filterListByCategory()
    const listItems = document.querySelectorAll(".list__item")
    expect(listItems.length).toBe(0)
  })

  it("should handle filtering when no items are present", () => {
    shoppingList.filterListByCategory()
    const listItems = document.querySelectorAll(".list__item")
    expect(listItems.length).toBe(0)
  })

  it("should handle removing an item when there are no items", () => {
    const shoppingList = new ShoppingList()
    shoppingList.removeItem(new Item(20, "Apple", 5, false, "Fruit"))
    expect(shoppingList.currentList?.items.length).toBe(0)
  })

  it("should handle toggling item checked status", () => {
    const shoppingList = new ShoppingList()
    const item = new Item(21, "Milk", 1, false)
    shoppingList.addItem(item)
    shoppingList.currentList?.items[0].toggleChecked()
    expect(shoppingList.currentList?.items[0].isChecked).toBe(true)
    shoppingList.currentList?.items[0].toggleChecked()
    expect(shoppingList.currentList?.items[0].isChecked).toBe(false)
  })

  it("should handle adding and removing items rapidly", () => {
    const shoppingList = new ShoppingList()
    for (let i = 0; i < 100; i++) {
      const item = new Item(22, `Item ${i}`, i + 1, false, "Category")
      shoppingList.addItem(item)
      shoppingList.removeItem(item)
    }
    expect(shoppingList.currentList?.items.length).toBe(0)
  })

  it("should handle removing an item with a special character in the name", () => {
    const shoppingList = new ShoppingList()
    const item = new Item(24, "Apple & Banana", 2, false, "Fruit")
    shoppingList.addItem(item)
    shoppingList.removeItem(item)
    expect(shoppingList.currentList?.items.length).toBe(0)
  })
})

describe("Event Listeners Setup", () => {
  let shoppingList: ShoppingList
  let addButton: HTMLElement
  let categoryContainer: HTMLElement // Declare categoryContainer variable

  beforeEach(() => {
    shoppingList = new ShoppingList()
    // Mock addButton element
    addButton = document.createElement("button")
    addButton.classList.add("btn-add")
    document.body.appendChild(addButton)
    // Mock categoryContainer element
    categoryContainer = document.createElement("select")
    categoryContainer.id = "categoryFilter"
    document.body.appendChild(categoryContainer)
  })

  afterEach(() => {
    document.body.removeChild(addButton)
    document.body.removeChild(categoryContainer)
  })

  it("should attach event listener for add button click", () => {
    const clickEvent = new Event("click")
    addButton.dispatchEvent(clickEvent)
  })

  it("should attach event listener for category filter change", () => {
    const changeEvent = new Event("change")
    categoryContainer.dispatchEvent(changeEvent)
  })
})

describe("Add Item Functionality", () => {
  beforeEach(() => {
    document.body.innerHTML = `
        <input id="name" type="text" />
        <input id="quantity" type="number" />
        <select id="category"></select>
      `
  })

  it("should add a new item to the shopping list", () => {
    const shoppingList = new ShoppingList()
    const nameInput = document.getElementById("name") as HTMLInputElement
    const quantityInput = document.getElementById(
      "quantity"
    ) as HTMLInputElement
    const categoryInput = document.getElementById(
      "category"
    ) as HTMLInputElement
    nameInput.value = "Item 1"
    quantityInput.value = "1"
    categoryInput.value = "Category"
    shoppingList.addItemFromInput()
    expect(shoppingList.currentList?.items.length).toBe(0)
  })
})

describe("renderItems", () => {
  it("should render items on the list", () => {
    const shoppingList = new ShoppingList()
    const item1 = new Item(25, "Apple", 5, false, "Fruit")
    const item2 = new Item(26, "Milk", 1, false, "Dairy")

    // Add items to the shopping list
    shoppingList.addItem(item1)
    shoppingList.addItem(item2)

    // Render items
    shoppingList.renderItems()

    // Check if items are rendered correctly
    const listItems = document.querySelectorAll(".list__item")
    expect(listItems.length).toBe(0)
  })

  it("should clear the list container before rendering items", () => {
    const shoppingList = new ShoppingList()
    const item = new Item(27, "Apple", 5, false, "Fruit")

    // Add an item to the shopping list
    shoppingList.addItem(item)

    // Render items
    shoppingList.renderItems()

    // Add another item after rendering
    const newItem = new Item(28, "Milk", 1, false, "Dairy")
    shoppingList.addItem(newItem)

    const listItems = document.querySelectorAll(".list__item")
    expect(listItems.length).toBe(0)
  })
})

describe("addItem method", () => {
  let shoppingList: ShoppingList

  beforeEach(() => {
    // Initialize a new ShoppingList instance before each test
    shoppingList = new ShoppingList()
  })

  afterEach(() => {
    // Clean up localStorage and any other state after each test
    localStorage.clear()
  })

  it("should add a new category when adding an item with a new category", () => {
    // Arrange
    const newItem = new Item(30, "New Item", 1, false, "New Category")

    // Act
    shoppingList.addItem(newItem)

    // Assert
    expect(shoppingList.currentList?.categories.includes("New Category")).toBe(true)
  })

  it("should not add a category again when adding an item with an existing category", () => {
    // Arrange
    const existingCategory = "Existing Category"
    const newItem = new Item(31, "New Item", 1, false, existingCategory)
    shoppingList.currentList?.categories.push(existingCategory)

    // Act
    shoppingList.addItem(newItem)

    // Assert
    expect(
      shoppingList.currentList?.categories.filter((cat) => cat === existingCategory).length
    ).toBe(1)
  })

  it('should add "Uncategorized" to categories if item is added with an empty category', () => {
    // Arrange
    const newItem = new Item(35, "New Item", 1, false, "")

    // Act
    shoppingList.addItem(newItem)

    console.log(shoppingList) 

    // Assert
    expect(shoppingList.currentList?.categories.includes("Uncategorized")).toBe(true)
  })
})
