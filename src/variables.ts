export const body = document.querySelector("body") as HTMLBodyElement;

export const header = {
  header: document.querySelector(".main__content") as HTMLDivElement,
  headerMain: document.querySelector(".main__content-header") as HTMLDivElement,
  headerBox: document.querySelector(".main__content-inputs") as HTMLDivElement,
  headerTitle: document.querySelector(".main__content-header-title") as HTMLSpanElement,
  headerBtnEdit: document.getElementById("editList") as HTMLButtonElement,
  headerBtnDelete: document.getElementById("deleteList") as HTMLButtonElement,
  headerBtnToggle: document.getElementById("toggle") as HTMLButtonElement,
}

export const sidebar = {
  sidebar: document.querySelector(".sidebar") as HTMLUListElement,
  sidebarList: document.querySelector(".sidebar__lists") as HTMLUListElement,
  sidebarBtnCreate: document.getElementById("new-list") as HTMLButtonElement,
  sidebarBtnClose: document.getElementById("close") as HTMLButtonElement,
}

export const modal = {
  overlay: document.getElementById("overlay") as HTMLDivElement,

  modalForm: document.getElementById("form") as HTMLFormElement,
  modalTitle: document.getElementById("modal-title") as HTMLHeadingElement,
  modalInputText: document.getElementById("modal-text") as HTMLInputElement,
  modalBtnCancel: document.getElementById("cancel") as HTMLButtonElement,
}

export const nav = {
  navInputName: document.getElementById("name") as HTMLInputElement,
  navInputQuantity: document.getElementById("quantity") as HTMLInputElement,
  navInputCategory: document.getElementById("category") as HTMLInputElement,
  navBtnAdd: document.querySelector(".btn-add") as HTMLButtonElement,
  navSelectCategory: document.getElementById("categoryFilter") as HTMLSelectElement,
}

export const content = {
  contentList: document.querySelector(".main__content-list") as HTMLUListElement,
}